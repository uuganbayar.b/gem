<?php $the_query = new WP_Query( 'page_id=20' ); ?>

<?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
<ul class="social-menu <?php
if ( is_home() ) {
    echo "home";
}
?>">
    <li><a href="<?php echo get_field( "facebook_url" ); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
    <li><a href="<?php echo get_field( "instagram_url" ); ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
    <li><a href="<?php echo get_field( "linkedin_url" ); ?>" target="_blank"><i class="fab fa-linkedin"></i></a></li>
</ul>


<?php endwhile;?>