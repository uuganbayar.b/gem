<?php /* Template Name: Human resource page */ ?>

<?php get_header(); ?>

<div id="primary" class="content-area page">

    <main id="main" class="container" role="main">
        <?php query_posts('cat=13');
if ( have_posts() ) : 
while ( have_posts() ) : the_post(); ?>
        <div class="post-grid">
            <div class="row show-grid flex">
                <div class="col-md-6">
                    <div class="post" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
                        <h4><?php the_title(); ?></h4>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail post-featured"
                        style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)">
                    </div>
                </div>
            </div>
        </div>

        <?php endwhile; endif; 				
        wp_reset_query(); 
 ?>
        <div class="quote relative">
            <img data-aos="fade" data-aos-delay="1000" data-aos-duration="1000" class="quote1"
                src=<?php echo get_template_directory_uri()."/assets/images/quote1.png" ?> />
            <blockquote data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000">
                <?php the_content(); ?>
            </blockquote>
            <img data-aos="fade" data-aos-delay="1000" data-aos-duration="1000" class="quote2"
                src=<?php echo get_template_directory_uri()."/assets/images/quote2.png" ?> />
        </div>




        <h4 data-aos="fade-down" data-aos-duration="1000" class="timeline-title">
            <?php echo get_cat_name( $category_id = 14 );?></h4>

        <div class="wrapper center-block">
            <div  data-aos="fade-up" data-aos-duration="1000" class="panel-group vacancy" id="accordion" role="tablist" aria-multiselectable="true">

                <?php

            $parent_cat_arg = array('hide_empty' => false, 'parent' => 14 );
            $parent_cat = get_terms('category',$parent_cat_arg);//category name

            foreach ($parent_cat as $catVal) {

                ?>

                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapse<?php echo $catVal->term_id ?>" aria-expanded="true"
                                aria-controls="collapse<?php echo $catVal->term_id ?>">
                                <?php echo $catVal->name ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse<?php echo $catVal->term_id ?>" class="panel-collapse collapse in" role="tabpanel"
                        aria-labelledby="headingOne">
                        <div class="panel-body">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Албан тушаал</th>
                                        <th>Хэлтэс</th>
                                        <th>Анкет</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                            query_posts('cat='. $catVal->term_id.'');
                            if (have_posts()) :
                            while (have_posts()) : the_post();
                            ?>
                            <tr>
                                <td><?php the_title(); ?>
                                </td>
                                <td><?php echo get_field('department'); ?>
                                </td>
                                <td>
                                    <a href="#">Онлайн анкет бөглөх</a>
                                </td>
                            </tr>

                            <?php
                            endwhile;
                                endif; 				
                                wp_reset_query(); 
                                    ?>

                                </tbody>
                            </table>
                           

                            
                        </div>

                    </div>
                </div>

                <?php    }
                    ?>




                <!-- <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Collapsible Group Item #2
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Collapsible Group Item #3
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                        aria-labelledby="headingThree">
                        <div class="panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad
                            squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa
                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid
                            single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                            beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice
                            lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you
                            probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div> -->
            </div>
        </div>








        <?php $the_query = new WP_Query( 'page_id=169' ); ?>

        <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
        <div class="phase" data-aos="fade-right" data-aos-duration="1000">
            <h4 class="timeline-title"><?php the_title(); ?></h4>
            <?php the_content(); ?>
        </div>
        <?php endwhile;?>
    </main><!-- .site-main -->
    <?php $the_query = new WP_Query( 'page_id=155' ); ?>

    <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
    <div class="row student-intern">
        <div class="col-md-6 no-padding">
            <div data-aos="fade-up" data-aos-duration="300"
                style="background:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)"
                id="parent-<?php the_ID(); ?>" class="bg-thumbnail">
            </div>

        </div>
        <div class="col-md-6 student-content">
            <div class="">
                <h4><?php the_title(); ?></h4>
                <div class="content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php endwhile;?>
</div><!-- .content-area -->

<?php get_footer(); ?>