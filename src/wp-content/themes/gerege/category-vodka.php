<?php get_header(); ?>

<div class="bg-thumbnail product-page"
    style="background: url(<?php echo get_template_directory_uri()."/assets/images/product-bg.png" ?>)">
    <?php $the_query = new WP_Query( 'page_id=122' ); ?>

    <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>

    <div data-aos="fade-down" data-aos-duration="1000" class="bg-thumbnail product-page-bg"
        style="background:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)">

    </div>
    <div class="container product-page-content" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
        <h4><?php the_title(); ?></h4>
        <p><?php the_content(); ?></p>
    </div>


    <?php endwhile; ?>

    <div class="container-mid container" id=" content" role="main">

        <h4 class="container timeline-title" data-aos="fade-down" data-aos-duration="1000">Бүтээгдэхүүн</h4>
        <div class="product-slider" data-aos="fade-down" data-aos-duration="1000">
            <?php query_posts('post_type=products'); ?>
            <?php 
            if ( have_posts() ) : 
            while ( have_posts() ) : the_post(); ?>
            <div class="product-item">
                <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>" />
                <span><?php the_title(); ?></span>
            </div>

            <?php endwhile; // End Loop
            ?>
        </div>
        <?php
        
            else: ?>
        <p>Sorry, no posts matched your criteria.</p>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>

    <?php $the_query = new WP_Query( 'page_id=122' ); ?>

    <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>

    <div data-aos="fade-down" data-aos-duration="1000"
        class="page-video bg-thumbnail product-page-bg product-page-video"
        style="background:linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.3)), url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)">
        <img data-toggle="modal" data-target="#exampleModal"
            src=<?php echo get_template_directory_uri()."/assets/images/play.png" ?> />
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <?php the_field('video_url'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php endwhile; ?>
    </section>

    <?php get_footer(); ?>