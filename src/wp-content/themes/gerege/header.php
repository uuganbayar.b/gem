<!DOCTYPE html>
<html>

<head>
  <!-- <title>Gem</title> -->
  <?php 
wp_head();
 ?>
     <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link href='https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap' rel='stylesheet' />
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
</head>

<body>
  <div class="main-header   <?php
if ( is_home() ) {
    echo "home";
} else {
    echo "non-home";
}
?>">
    <nav class="navbar navbar-expand-lg">
      <a class="navbar-brand logo" href="/">
        <?php if(function_exists('the_custom_logo')){
          the_custom_logo();
        } ?>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <?php 
    // menu
      wp_nav_menu(
        array(
          'menu' => 'primary',
          'container' => '',
          'theme_location' => 'primary',
          'items_wrap' => '<ul id="" class="navbar-nav mr-auto">%3$s</ul>'

        )
      )
      ?>

        <?php  get_template_part('template-parts/search', 'form'); ?>
        <?php echo do_shortcode( '[language-switcher]' ); ?>
      </div>
    </nav>
  </div>
  <main class="relative">