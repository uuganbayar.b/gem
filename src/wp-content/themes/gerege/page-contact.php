<?php /* Template Name: Contact page */ ?>

<?php get_header(); ?>

<div id="primary" class="content-area page contact">
    <main id="main" class="container page" role="main" >
        <ul data-aos="fade-down" data-aos-delay="100" data-aos-duration="1000" class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link " id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                    aria-controls="contact" aria-selected="true">Холбоо барих</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="branches-tab" data-toggle="tab" href="#branches" role="tab"
                    aria-controls="branches" aria-selected="false">Борлуулалтын бүсүүдтэй холбоо барих</a>
            </li>
        </ul>
        <div data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000" class="tab-content" id="myTabContent">
            <div class="tab-pane fade " id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <div class="row contact-content">
                    <div class="col-md-6">

                        <ul class="contact-info">
                            <li><label>Утас:</label><span><?php echo get_field( "number" ); ?></span></li>
                            <li><label>Мэйл:</label><span><?php echo get_field( "mail" ); ?></span></li>
                            <li><label>Вэб:</label><span><?php echo get_field( "web" ); ?></span></li>
                            <li><label>Хаяг:</label><span><?php echo get_field( "address" ); ?></span></li>
                        </ul>
                        <?php  include get_theme_file_path( 'includes/social.php' ); ?>
                    </div>
                    <div class="col-md-6">
                        <?php
                        the_content();
                        ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show active" id="branches" role="tabpanel" aria-labelledby="branches-tab">
                <div class="row contact-content">
                    <div class="col-md-8 map-container">
                        <?php  include get_theme_file_path( 'includes/svgmap.php' ); ?>
                    </div>
                    <div class="col-md-4 branch-info">
                        <h5>Салбар байршил сонгох</h5>
                    </div>
                </div>
            </div>
        </div>


    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>