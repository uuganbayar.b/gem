<?php get_header(); ?>

<section id="primary" class="site-content page news">
    <div class="container" id=" content" role="main">
        
        <?php 
if ( have_posts() ) : 
while ( have_posts() ) : the_post(); ?>
        <div class="post-grid"  >
            <div class="row show-grid flex">
                <div class="col-md-6">
                    <div class="post" data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000">
                        <h4><?php the_title(); ?></h4>
                        <div class="content">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="thumbnail post-featured" style="background: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)">
                    </div>
                </div>
            </div>
        </div>

        <?php endwhile; // End Loop
 ?>

        <?php
            gerege_pagination(); ?>
        <?php
        
else: ?>
        <p>Sorry, no posts matched your criteria.</p>
        <?php endif; ?>

    </div>
</section>

<?php get_footer(); ?>