<?php get_header(); ?>

<div class="home-slider">
    <?php

$args = array(
    'post_type' => 'page',
    'posts_per_page' => -1,
    'post_parent' => 56,
    'order' => 'ASC',
    'orderby' => 'menu_order'
    );


    $parent = new WP_Query( $args );

    if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

    <div style="background: linear-gradient(rgba(0,0,0,0.2), rgba(0,0,0, 0.2)), url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)"
        id="parent-<?php the_ID(); ?>" class="bg-thumbnail parent-page">

        <div class="item">
            <div class="caption-wrapper">
                <h1><?php the_title(); ?></h1>
                <ul class="link-list">
                    <li>
                        <a>Жэм вэй</a>
                    </li>
                    <li>
                        <a>Цагийн хэлхээс</a>
                    </li>
                  </ul>
            </div>
        </div>

    </div>

    <?php endwhile; ?>

    <?php endif; wp_reset_postdata(); ?>
</div>
<?php  include get_theme_file_path( 'includes/social.php' ); ?>

<?php get_footer(); ?>