$(document).ready(function () {
  $('.home-slider').slick({
    infinite: false,
    slidesToShow: 1,
    dots: true,
    slidesToScroll: 1,
    vertical: true,
    verticalSwiping: true,
    appendDots: $(this).siblings('dots-container'),
    prevArrow:
      '<button type="button" class="slick-prev"><i class="fas fa-long-arrow-alt-left"></i></button>',
    nextArrow:
      '<button type="button" class="slick-next"><i class="fas fa-long-arrow-alt-right"></i></button>',
  });
  $('.timeline-carousel').slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    prevArrow:
      '<button type="button" class="timeline-prev"><i class="fas fa-long-arrow-alt-left"></i></button>',
    nextArrow:
      '<button type="button" class="timeline-next"><i class="fas fa-long-arrow-alt-right"></i></button>',
  });

  $('.product-slider').slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
  });

  $('.timeline-item').click(function () {
    $('.timeline-item.active').removeClass('active');
    $(this).addClass('active');
    let id = $(this).attr('id');
    let idContent = $('.timeline-content .item').attr('id');
    console.log(id);
    if (id === idContent) {
      // $('.timeline-content .item').show();
      console.log(id);
    }
  });

  $('#to-top').click(function () {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  });

  $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
});
