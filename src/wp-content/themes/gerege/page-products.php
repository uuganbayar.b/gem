<?php /* Template Name: Products page */ ?>

<?php get_header(); ?>

<div id="primary" class="content-area page products-page">
    <main id="main" class="container" role="main">
        <div class="row">


        <?php

$parent_cat_arg = array('hide_empty' => false, 'parent' => 5, 'order' => 'ASC',
'orderby' => 'modified' );
$parent_cat = get_terms('category',$parent_cat_arg);//category name
// print_r($parent_cat);
foreach ($parent_cat as $catVal) {
    ?>
        <div class="col-md-6">
        <a href="<?php echo get_site_url(). "/category/products/".$catVal->slug ?>">
            <div data-aos="fade-left" data-aos-delay="100" data-aos-duration="1000" class="cat-item bg-thubmnail"
                style="background: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.5)), url(<?php echo z_taxonomy_image_url($catVal->term_id); ?>)">
                <h2><?php echo $catVal->name ?> <i class="fas fa-long-arrow-alt-right"></i></h2>
            </div></a>
        </div>

        <?php
}
?>
        </div>

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>