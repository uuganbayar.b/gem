<div class="search-wrap">
    <form role="search" method="get" id="searchform" class="searchform form-inline my-2 my-lg-0"
        action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div>
            <input placeholder="Хайх" class="search mr-sm-2" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" />
            <i class="fas fa-search"></i>
        </div>
    </form>
</div>