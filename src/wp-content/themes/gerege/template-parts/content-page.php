
<article class="page content">
    <h1 class="page-title"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h1>
    <div class="meta-data">
        <span><?php the_date(); ?> </span>
        <span><?php the_tags('<span>','</span><span>','</span>'); ?> </span>
    </div>
    <div class="content">
        <?php the_content(); ?>
</article>