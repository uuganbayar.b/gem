<?php

function gerege_theme_support(){
    //add dynamic title
    add_theme_support('title-tag');
    add_theme_support('custom-logo');
    add_theme_support('post-thumbnails');

}

add_action('after_setup_theme', 'gerege_theme_support');

function gerege_menus(){
    $locations = array(
        'primary' => "Main Menu",
        'footer' => "Footer menu"
    );

    register_nav_menus($locations);

}

add_action('init', 'gerege_menus');

function gerege_register_styles(){
    
    wp_enqueue_style('gerege-bootstrap', get_template_directory_uri() . "/assets/css/bootstrap.min.css", array(), '4.5.2', 'all');
    wp_enqueue_style('fontawesome-style', get_template_directory_uri() . "/assets/css/fontawesome/css/all.min.css", array(), '1.0', 'all');
    wp_enqueue_style('gerege-style', get_template_directory_uri() . "/style.css", array(), '1.0', 'all');
    wp_enqueue_style('custom-style', get_template_directory_uri() . "/assets/css/main.min.css", array(), '1.0', 'all');
}

add_action('wp_enqueue_scripts', 'gerege_register_styles');

// adding js
function gerege_register_scripts(){

    
    wp_enqueue_script('bootstrap-slim', 'https://code.jquery.com/jquery-3.5.1.slim.min.js', array(), '3.5.1', true);
    wp_enqueue_script('bootstrap-popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js', array(), '1.16.1', true);
    wp_enqueue_script('bootstrap-min', get_template_directory_uri() . "/assets/js/bootstrap.min.js", array(), '4.5.2', true);
    wp_enqueue_script('gerege-main', get_template_directory_uri() . "/assets/js/main.js", array(), '1.0', true);
}

add_action('wp_enqueue_scripts', 'gerege_register_scripts');

//pagination
function gerege_pagination() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}

function custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Products', 'Post Type General Name', 'twentythirteen' ),
            'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'twentythirteen' ),
            'menu_name'           => __( 'Products', 'twentythirteen' ),
            'parent_item_colon'   => __( 'Parent Product', 'twentythirteen' ),
            'all_items'           => __( 'All Products', 'twentythirteen' ),
            'view_item'           => __( 'View Product', 'twentythirteen' ),
            'add_new_item'        => __( 'Add New Product', 'twentythirteen' ),
            'add_new'             => __( 'Add New', 'twentythirteen' ),
            'edit_item'           => __( 'Edit Product', 'twentythirteen' ),
            'update_item'         => __( 'Update Product', 'twentythirteen' ),
            'search_items'        => __( 'Search Product', 'twentythirteen' ),
            'not_found'           => __( 'Not Found', 'twentythirteen' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'Products', 'twentythirteen' ),
            'description'         => __( 'Product news and reviews', 'twentythirteen' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
             
            // This is where we add taxonomies to our CPT
            'taxonomies'          => array( 'category' ),
        );
         
        // Registering your Custom Post Type
        register_post_type( 'Products', $args );
     
    }
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'custom_post_type', 0 );