<?php get_header();?>
<?php while ( have_posts() ) : the_post(); ?>
	<div class="container">
		<div class="clear"></div>
		<div class="main">

				<div class="content">
					<?php get_template_part('template-parts/content', 'article'); ?>
				</div>
		</div>
		<div class="clear"></div>
	</div>
<?php endwhile; ?>
<?php get_footer();?>