<?php ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="container page" role="main">
        <?php
            get_template_part( 'template-parts/content', 'page' );
 
        ?>

    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>