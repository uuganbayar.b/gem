<?php get_header();?>
<div class="bg-thumbnail product-page" style="background: url(<?php echo get_template_directory_uri()."/assets/images/product-bg.png" ?>)">

<?php while ( have_posts() ) : the_post(); ?>
	<div class="container">
		<div class="clear"></div>
		<div class="main">
			<p>product</p>
				<div class="content">
					<?php get_template_part('template-parts/content', 'article'); ?>
				</div>
		</div>
		<div class="clear"></div>
	</div>
<?php endwhile; ?>
</div>
<?php get_footer();?>