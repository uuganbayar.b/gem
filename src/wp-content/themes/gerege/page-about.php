<?php /* Template Name: About page */ ?>

<?php get_header(); ?>

<div id="primary" class="content-area contact-page">
    <?php $the_query = new WP_Query( 'page_id=86' ); ?>

    <?php while ($the_query -> have_posts()) : $the_query -> the_post();  ?>
    <div data-aos="fade-up" data-aos-duration="300" style="background:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>)"
        id="parent-<?php the_ID(); ?>" class="bg-thumbnail page-video">
        <img data-toggle="modal" data-target="#exampleModal"
            src=<?php echo get_template_directory_uri()."/assets/images/play.png" ?> />
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php endwhile;?>
    <main id="main" class="container page" role="main">
        <h4 data-aos="fade-down" data-aos-duration="1000" class="timeline-title"><?php echo get_cat_name( $category_id = 4 );?></h4>

        <div data-aos="fade-up" data-aos-delay="100" data-aos-duration="1000" class="timeline-carousel">
            <?php 
	query_posts('cat=4');
        if (have_posts()) :
        while (have_posts()) : the_post();
?>
            <div class="timeline-item" id="<?php echo get_the_ID() ?>">
                <?php echo get_the_post_thumbnail($post->ID); ?>
                <h6><?php echo get_field( "year" ); ?></h6>
            </div>

            <?php 
	endwhile;
        endif; 				
        wp_reset_query(); 
?>
            <?php 
	query_posts('cat=4');
        if (have_posts()) :
        while (have_posts()) : the_post();
?>
            <div class="timeline-item">
                <?php echo get_the_post_thumbnail($post->ID); ?>
                <h6><?php echo get_field( "year" ); ?></h6>
            </div>

            <?php 
	endwhile;
        endif; 				
        wp_reset_query(); 
?>
            <?php 
	query_posts('cat=4');
        if (have_posts()) :
        while (have_posts()) : the_post();
?>
            <div class="timeline-item">
                <?php echo get_the_post_thumbnail($post->ID); ?>
                <h6><?php echo get_field( "year" ); ?></h6>
            </div>

            <?php 
	endwhile;
        endif; 				
        wp_reset_query(); 
?>
            <?php 
	query_posts('cat=4');
        if (have_posts()) :
        while (have_posts()) : the_post();
?>
            <div class="timeline-item">
                <?php echo get_the_post_thumbnail($post->ID); ?>
                <h6><?php echo get_field( "year" ); ?></h6>
            </div>

            <?php 
	endwhile;
        endif; 				
        wp_reset_query(); 
?>
        </div>
        <div class="timeline-content">
            <?php 
	query_posts('cat=4');
        if (have_posts()) :
        while (have_posts()) : the_post();
?>
            <div class="item" id="<?php echo get_the_ID() ?>">
                <div class="row">
                    <div class="col-md-7">
                        <h6><?php the_title(); ?></h6>
                        <p><?php the_content(); ?></p>
                    </div>
                    <div class="col-md-5">
                        <span><?php echo get_field( "date" ); ?></span>
                    </div>
                </div>
            </div>

            <?php 
	endwhile;
        endif; 				
        wp_reset_query(); 
?>
        </div>
        <div id="react-container"></div>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>