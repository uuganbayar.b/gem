FROM wordpress:5.5.0-php7.4-apache

COPY --chown=www-data:www-data src/wp-content/plugins/ /usr/src/wordpress/wp-content/plugins
COPY --chown=www-data:www-data src/wp-content/themes/ /usr/src/wordpress/wp-content/themes